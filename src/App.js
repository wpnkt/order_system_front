import React from 'react';
import './App.css';

import Topbar from './components/Topbar/Topbar';
import Primary from './components/Primary/Primary';
import Footer from './components/Footer/Footer';

import { BrowserRouter, Route } from "react-router-dom";

const Posts = () => {
  return (
    <h1>POSTS</h1>
  );
};

const Posts2 = () => {
  return (
    <h1>POSTS22</h1>
  );
};

class App extends React.Component{
  state = {
    header: <Topbar />,
    primary: <Primary />,
    footer: <Footer />
  }

  render(){
    return (
      <BrowserRouter>
        <Route path="/posts" component={Posts} />
        <Route path="/posts2" component={Posts2} />
      </BrowserRouter>
    );
  }

}

export default App;
